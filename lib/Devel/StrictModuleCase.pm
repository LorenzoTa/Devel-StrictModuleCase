package Devel::StrictModuleCase;

use strict;
use warnings;
use File::Spec;
use version;
use Devel::OverrideGlobalRequire;


our $VERSION = '0.1';

our $debug = $ENV{PERL_STRICT_MODULE_CASE} ? $ENV{PERL_STRICT_MODULE_CASE} : 0;

# see https://metacpan.org/pod/release/DAGOLDEN/Acme-require-case-0.013/lib/Acme/require/case.pm
# see https://github.com/haarg/require-case


Devel::OverrideGlobalRequire::override_global_require( sub { 
	
	print "\nargs received: [",( ref $_[0] eq 'CODE' ? 'CODE' : $_[0] ),"] [$_[1]]\n" if $debug;
	
	# check if a version number was passed
	if ( eval { version->parse( $_[1] ) } ){
		print "SKIPPING ANALISYS OF $_[1]..\n" if $debug;
		my $ret = $_[0]->();
		return $ret;
	}
	# see https://perldoc.perl.org/perlfunc#require
	if (exists $INC{$_[1]}) {
		print "[$_[1]] ALREADY IN \%INC...\n" if $debug;
        return 1 if $INC{$_[1]};
    }
	
	my $original_filename = $_[1];
	print "PASSED IN FILENAME: [$original_filename]\n" if $debug;
	my @moduledirs = split /\/|\\/,$original_filename;
	my $modulefile = pop @moduledirs;
	print "module dirs: [",(join ' ',@moduledirs),"] module file: $modulefile\n" if $debug;
	
	# @INC lookup
	foreach my $prefix (@INC) {
		# both must drop to zero
		my ($ok_filename, $ok_dirs) = (1, 0 + @moduledirs );
		if (ref($prefix)) {
			# ... do other stuff - ????? ....
			# https://perldoc.perl.org/perlfunc#require
		}
		my $full_filename_path = "$prefix/$original_filename";
		$full_filename_path = File::Spec->rel2abs( $full_filename_path ) unless File::Spec->file_name_is_absolute( $full_filename_path );
		print "\tCANDITATE FULL PATH: [$full_filename_path]\n" if $debug;
		my ($vol, $dirs, $file) = File::Spec->splitpath($full_filename_path);
		unless ( -d File::Spec->catpath($vol, $dirs) ){
			print "\t\tSkipping non existing dir..\n" if $debug;
			next;
		}

		# check filename to be correctly cased
		opendir my $dh, File::Spec->catpath($vol, $dirs) or die $!;
		while ( my $cur_file = readdir $dh ){
			next unless -f File::Spec->catfile( $vol, $dirs, $cur_file );
			print "\t\tFILE: [$cur_file]\n" if $debug;
			if ( $file eq $cur_file ){
			print "\tFOUND [$cur_file] eq to [$file]\n" if $debug;
				$ok_filename--;
				last;
			}
			elsif ( $file =~ /^\Q$cur_file\E$/i ){
				print "\tPROBLEM !!!!! FOUND [$cur_file] INSENSITIVE MATCHING [$file]\n" if $debug;
				die "Found a case problem in the filename: [$_[1]]\n",
					"You attempted to load '$file' instead of the correct '$cur_file' at ",
						(join ' line ',(caller(2))[1,2]),"\n";
			}
		}
		# check dirs to be correctly cased
		my @all_dirs = split /\/|\\/,$dirs;
		# but not all: only those coming from the module name
		# so remove the last because it was checked for filename
		while (my $last_dir = pop @moduledirs){
		pop @all_dirs;
			my $dirpath = File::Spec->catpath($vol, join '/',@all_dirs);
			print "\tOPENING DIR: $dirpath\n" if $debug;
			opendir my $dh, $dirpath or die $!;
			while ( my $cur_dir = readdir $dh ){
				next unless -d File::Spec->catpath('',$dirpath,$cur_dir);
				next if $cur_dir eq '.' or $cur_dir eq '..';
				print "\t\tDIR: [$cur_dir]\n" if $debug;
				if ( $cur_dir eq $last_dir ){
					print "\tFOUND [$cur_dir] eq to [$last_dir]\n" if $debug;
					$ok_dirs--;
					last;
				}
				elsif ( $last_dir =~ /^\Q$cur_dir\E$/i ){
					print "\tPROBLEM !!!!! FOUND DIR [$cur_dir] INSENSITIVE MATCHING [$last_dir]\n" if $debug;
					die "Found a case problem in one directory of [$_[1]]\n",
						"You attempted to load from '$last_dir' instead of the correct form: '$cur_dir' at ",
						(join ' line ',(caller(2))[1,2]),"\n";
				}
			}
		
		}
		
		if ( 0 == $ok_filename and 0 == $ok_dirs ){
		
			print "---->[$original_filename] OK FILE: $ok_filename OK DIRS: $ok_dirs\n" if $debug;
			my $ret = $_[0]->();
			# my $ret = require $full_filename_path; #?? as we already done the lookup
			return $ret;
		}
	}
	die "Can't locate $original_filename in \@INC ";
}
);



1;

__DATA__
=pod

=head1 NAME

Devel::StrictModuleCase - check if a module was loaded with a wrong name or path in case insensitive filesystems

=head1 SYNOPSIS

	perl -MDevel::StrictModuleCase -MDATA::Dumper -e 1
	
	# Found a case problem in one directory of [DATA/Dumper.pm]
	# You attempted to load from 'DATA' instead of the correct form: 'Data' at -e line 0
	# BEGIN failed--compilation aborted.

	perl -MDevel::StrictModuleCase -MData::dUMPER -e 1
	
	# Found a case problem in the filename: [Data/dUMPER.pm]
	# You attempted to load 'dUMPER.pm' instead of the correct 'Dumper.pm' at -e line 0
	# BEGIN failed--compilation aborted.
	 

=head1 DESCRIPTION

This is an experiment: use a better module: L<Acme::require::case> or L<https://github.com/haarg/require-case> because better implemented and both support version numbers after the module
like C<use MCE 1.3>



=head1 AUTHOR

LORENZO, C<< <LORENZO at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-devel-strictmodulecase at rt.cpan.org>, or through
the web interface at L<https://rt.cpan.org/NoAuth/ReportBug.html?Queue=Devel-StrictModuleCase>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Devel::StrictModuleCase


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=Devel-StrictModuleCase>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Devel-StrictModuleCase>

=item * CPAN Ratings

L<https://cpanratings.perl.org/d/Devel-StrictModuleCase>

=item * Search CPAN

L<https://metacpan.org/release/Devel-StrictModuleCase>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2021 LORENZO.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of Devel::StrictModuleCase
