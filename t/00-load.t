#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Devel::StrictModuleCase' ) || print "Bail out!\n";
}

diag( "Testing Devel::StrictModuleCase $Devel::StrictModuleCase::VERSION, Perl $], $^X" );
